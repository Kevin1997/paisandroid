package com.example.paises;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.paises.Recycler.adaptador;
import com.example.paises.enetity.pais;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<pais> ListDatos;
    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycler=(RecyclerView) findViewById(R.id.recycler1);
       recycler.setLayoutManager(new LinearLayoutManager(this));
       // recycler.setLayoutManager(new GridLayoutManager(this,2));

        ListDatos=new ArrayList<>();
        llenarDatos();

        adaptador adapter=new adaptador(ListDatos,this);
        recycler.setAdapter(adapter);

    }

    private void llenarDatos() {
        ListDatos.add(new pais("España","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/zi6VIw2GgkE\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/zi6VIw2GgkE","https://iterin.com/18042-thickbox_default/bandera-espanola-con-escudo.jpg"));

        ListDatos.add(new pais("Costo Rica","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/kyQjSwIuSz4\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/kyQjSwIuSz4","https://cdn.webshopapp.com/shops/66605/files/264271562/bandera-de-costa-rica.jpg"));

        ListDatos.add(new pais("Chile","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/ml7pHgoLkFU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=ml7pHgoLkFU","https://assets.metrolatam.com/cl/2016/07/01/normal_1820__Bandera_Ejercito_Libertador_del_Peru-1-1200x800.jpg"));

        ListDatos.add(new pais("Argentina","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/ttXRKGPjek0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/ttXRKGPjek0","https://images-na.ssl-images-amazon.com/images/I/6165Jifl3qL._SX425_.jpg"));

        ListDatos.add(new pais("Bolivia","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/gvasn6xcqdE\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/gvasn6xcqdE","https://www.eldiario.net/noticias/2015/2015_08/nt150817/f_2015-08-17_34.jpg"));

        ListDatos.add(new pais("Brazil","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/vSDqD_G4_ms\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/vSDqD_G4_ms","https://www.banderas-mundo.es/data/flags/ultra/br.png"));

        ListDatos.add(new pais("Canadá","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/Ho75aZNiNFA\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=Ho75aZNiNFA","https://www.ecured.cu/images/5/5b/Bandera_Canada.png"));

        ListDatos.add(new pais("Colombia","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/SIZywhbgukk\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/SIZywhbgukk","https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Colombia.svg/1200px-Flag_of_Colombia.svg.png"));

        ListDatos.add(new pais("Cuba","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/Ngu3F429b4g\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/Ngu3F429b4g","https://www.comprarbanderas.es/images/banderas/400/8-cuba_400px.jpg"));

        ListDatos.add(new pais("Estados Unidos","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/SEdR9LuyuqU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/SEdR9LuyuqU","https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Hopkinson_Flag.svg/200px-Hopkinson_Flag.svg.png"));

        ListDatos.add(new pais("Mexico","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/cDAjDDdPqvA\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/cDAjDDdPqvA","https://www.comprarbanderas.es/images/banderas/400/121-mexico_400px.jpg"));

        ListDatos.add(new pais("Paraguay","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/kWYuTznD_AA\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/kWYuTznD_AA","https://www.ngenespanol.com/wp-content/uploads/2018/08/%C2%BFSabes-por-qu%C3%A9-la-bandera-de-Paraguay-es-%C3%BAnica-en-Am%C3%A9rica.jpg"));

        ListDatos.add(new pais("Uruguay","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/12-sxR2FcBs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/12-sxR2FcBs","https://www.banderas-mundo.es/data/flags/ultra/uy.png"));

        ListDatos.add(new pais("Ecuador","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/LzsdNotN8jg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=LzsdNotN8jg","https://i0.wp.com/estudiantes.mas.ec/wp-content/uploads/sites/44/2018/01/bandera-ecuador-bandera-tricolor.jpg"));

        ListDatos.add(new pais("Peru","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/D_YcCGbdht8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/D_YcCGbdht8","https://images-na.ssl-images-amazon.com/images/I/31VOHa8h8fL._SX355_.jpg"));

        ListDatos.add(new pais("Venezuela","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/usyDbeY-2_w\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/usyDbeY-2_w","https://www.banderas-mundo.es/data/flags/ultra/ve.png"));

        ListDatos.add(new pais("El Salvador","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/UnWzKEO6WnM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/UnWzKEO6WnM","https://www.ipandetec.org/wp-content/uploads/2018/01/El-Salvador.png"));

        ListDatos.add(new pais("Jamaica","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/vBh0Azqkb4I\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/vBh0Azqkb4I","https://turismo.org/wp-content/uploads/2012/07/Bandera-de-Jamaica.png"));

        //

        ListDatos.add(new pais("Australia","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/xnfcB3iv7xI?list=TLPQMjAxMTIwMTlb_L0iTJd6nQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/xnfcB3iv7xI?list=TLPQMjAxMTIwMTlb_L0iTJd6nQ","data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT4AAACfCAMAAABX0UX9AAAAz1BMVEUBIWn////kACsAAF3jACDiAATiAAD75ufrcHvjABXzsbYAG2fU2OIACWKYn7gAAGL619sAHmi4vc4ABWEAAFnqVWYAF2WUFU6+w9LpACilqb3jAB7vACQtP3kmOXbxmKIAD2MAEWP29/ohNXSyt8lyfJ/JzdqMlLA2Rn3sbHrm6fCfpbzd4Oh+h6dLWIgAAE1ibZZXY48SK2/xn6aeeJNqdJrqXGzteIXpSl350NX74OPvipX3vcXtAADuABj2dn2cYIORADaii6Wlb40+TYE+t2FiAAAKSklEQVR4nO2d/X+bNh6AYUrcZoCpHMoSMnsCYzvFmNjefE27u267y///N50Exi/YBgkJIzt6fugn7YYQTyT0/kX713e9QG8ADI0eYJKLfr77aZe7n8k/moAhHQMMeuSiD19v1oncdF4/saTQBjedz5+KAh9YBIrRh+U9pPK+dG5yeSRjTp1nOiN5PgsC+9QCRegzQP+ovEvQxymQX9+pkncp+rgE8uo7XfIuQd/dE6dAPn3l8uTX9+Fjh08gj74qefLrw3nnE1hfX7W8S9DHKbCuPhp58utbN3ofb+sKrKePTl5vyNKBb4PNc9QVWEcfrTym4U9LcApk13dN8ghcAln17cgrudnlyCNwCGTTRymPbdZCAmoLZNHXujy3oXS12gLp9bUuT3MXDfqrJ5BWX/vycF6DZucPawik0yeDPFz49EaLn1ZDII2+tuXBTJod6qGd/uTCZm6kMQus1reVV5ZggyUP9ufAwok7gR7gUaBhgXm/OX9sAqv0tS4vzWMwHQA7wTdKbDCYBkyrMOwwCCzXJ4M8XPzIzIg5CvGf4YjkFzZY+FKoBZbqk0IeBo327jhCDd9Poxa4LNG3lEMexhnv3HJ8nmmwrcCyBjM4qS+QRR6uvjDYZlhg1bVKAc6wtxZ495Ry23n9VnRwSh/m+5fObXbl3RF54dIB5RkQ9ZjI3ha/sY1EJat1q5h4WSH6bcOf/6bW9+3P7WWHxXY6qbr5VIw/F8yC3RvPgKje85GSVIdTpY8XEW8pCJK4kGycADEVWNBjSqwPDsZHEh4PhPgT9JgS69OWg+f5bOLlJTD2JrP582ApIOV3oU+Dhusjx1sn6TnIdw1Bje8HMfz+tKfv6XdB6YobXAFShUkfdSxywHYrhn172J+YZP8j7kFBhDtKL3jcFonU95PU/CruQQEeITmag8cBSh87cJCN1fDobShwvqBtQeWI0+eu4qwZcuKVwBnntgWVI06fkfhZoYN+InCU3bagcgS++4wjP/HTtqByBOprhrYFlaP0caH0caH0caH0caH0caH0cSG9vhsxPBae+1FMsj/OL4RtQPxRDK/7/h5fxST7l9+QpJPAJZM/QbPCUs82s2BNn1kGdYIe82r0gajLsjgq6DGvRR8c6jHLdKqgx7x8fdAmlRZNdD1tPVxAtRRX/QQBzX+l0HeYTnnK59VnheEzQJDsF5sgwwZzk2pW1a7AWfaKj/Tp892aTufLd2p93147t/mFHz+QfwmXTtXtG9eWA11dj2euhbNlvjx7gf7AvySSb7Hal5cfpXq6zSxQ6tu7tJNdKtH5FzQnGTLzPwKLd0WETl4Yn9QXh5ckEOw97Jyzx0knrzd4Kdke+TI4UvNlFQjdnXfxA99Ll1YeMEo35+axbaqT4Xt0If7BfJPHwOFZjaOXV7mz/iwCjQX/4M4Ao53SZ/Zr72BjkUdxruMMAoH5xln+IHg297PoWahOQmzyqE4VNS4Q6JzbUKF20MrpwQwxp8Mqj/JMW1MCszeUv9Kj7FUPa/4C4HRsRsFO5Q2CeDxm3Y7ALo/6RGUjAuEc3BtZl4MEjfBBv+4OAgQwVrLOWGRb5O9s9irknehnUJ/nbUCgNQ28PkAAXzcBtj0ydbbZugL+pull77bUk8d0mly8QID77PGIPHXcDwPezq7VxQnNyDZK1t9CXXmMsQxECzT6e0lxjlPtkJRi8kBsx37ry2OOpCFYIOjupBMgvnEqfpQuwH2YWGdpdHnk1YjjQimQMhIJ2DmywTtOBYFHcgztKLynvYZS3smnqRFFSKBA15lsC5/LO78Vrs9vax5tN5JTXs0YVtQCyzOPxwp754W6gK/8oTxd6lcfzZRIaTGoGUGtQmB+69LyhPziWCFaOOedveGUxxG/j0pgWQfMnRQvJ/dkWmfkhlMeV/RIAwyrBJb2X5P5aOL18qF+8BBOZ6NFk0EeDuGUxxm7tErg19J3GTlrBcBifV38YlvIbzDEyDE45XFHzi0XSLHHBZGeX6RXVPSm4JQnIG5zmUCKHVZ2es5qpp8hPMYROOUJiRp+WiCFPnz/wDUAbkaShgMsHYNTnqCY9acE0uiLAjLth0dvo7Nvx8L6Pr12fn3M+PGVWd5G34/HXX4wfzHhmMDvnylOVNr6IC11ThiyzzhzV/e///llw39DD8MY9hJNyEV//LLPH+QfJ4glISzQKxD+r7I+wn6yLnSA/ayawX06cPeW/j3GYu12InJVsd6kSSHGlAzrvgCFkO38PLM9CIWe7X1voEkrzc11AA0n0ntAVFSD94Vha8+ks7joG7YSyIo/22nhm47IeYVshsp60G+hv3jxoH46UNZNV5W9OqxXmeao7YxcJutFpvB8G4CvCejj4embKTYqzvvBnQcL4DozoWFd3g/wDZE2wxqed3b/alhba2OSVdEM6lfJA1QvEh5Aw8HgrxvjWT/vIv91YU15t7i/a5wo3+KuYMcY4DHkQNXemqRbq5lCASh2SOcwmEIBvG/2tzPBZToDtL+pn36j5vtj5O+evEHZdsXdxXEX+KOz5+pSQF29l4DNunq2XWPn82IWSHp6F7WUO/khwqLuEKRBaWG+2S0d+EIfDLsR2z6X90baUyHfoLJxJUb52t8M4Uprj7KyKP33ndvEzndnh4nj5JuMx46T5LulJmoloQywOYMdbfe5T6L8J1V1yzGGxU15e6iqW4F97HDFphiqqlvFtvoeoKpuNSXVV1VdCk5WX1V1qXCKX0HLiNXMHxU+PKrPUJMFFEAwON54mANBX+K7ZpDtHZVH8ABqO3tyYzijsgiawejMp5MviuJZ+GPtx7NUNVim17GFDuNmHRIieRY/3Lk0/lwwq3aXIu5ruLyAQJZxkJ9E1eLWRIkce9fdhSxnEIxkbMYUcZcxQWyORX4arQbr9So7zHcyn/nI/iEGAMAB2ttiNJmG48OenzkOp5PR4k3D/xdoObYr7K/SQHVOoAd4KGTcg5UcO8HSL9/e2+ClWJOjF2DfC/wOLh/ADKYDYJPwfIkNBtNA6Edm+bEOSh91EKhzQEKC6+aIdPC9dA3Gl+K3mmO8HejjjcUqFrTay9wKtZ2hPdz5gT55elgpzu7n3TljWgsH5VN+kZ+/BNkO4jcO9LfdhECuqpvFUkkzBn24zqZkZ50sa1v8xpY846CU9XpHsHQ1d5n5k2qdozhCkmcclAKyKjskmXKHqT+JjopBkBRnNuJEppkMO83T+lixn4VjlabywuVBgEfSfHCFKBZKFr/2LR/Y+mk3pi9Nz2X4lsxnXS8vgbHXnc2Tt2Hb2drgkoPtCdr8HZHuvSSDc4Lhushy8llxz7GQ60rzy836LYvd1ux+IV3PRQOk6SUlULIBG7bl6av9V5290j2phm1Z8xa+9KRq1DKAOSpmCcyk6rlo5KMUeLQBHdyMSJYx/O47LGn3Er37CHCY1Vpch6U7Pn7MlFz2NHcVp109CGL2SGsKY7GeeoT4p5bzcolsJm7rflFGoVAoFAqFQqFQKBQKhUKhkJ7/A7v5NCFjcr0QAAAAAElFTkSuQmCC"));

        ListDatos.add(new pais("Nueva Zelanda","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=KHyO36gYZNo\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=KHyO36gYZNo","https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Flag_of_New_Zealand.svg/1200px-Flag_of_New_Zealand.svg.png"));

        ListDatos.add(new pais("Papúa Nueva Guinea","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=xmk_WVSZMKo\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=xmk_WVSZMKo","https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Flag_of_Papua_New_Guinea.svg/1200px-Flag_of_Papua_New_Guinea.svg.png"));

        ListDatos.add(new pais("Fiyi","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=gLn4NkuPsJw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=gLn4NkuPsJw","https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Fiji.svg/1200px-Flag_of_Fiji.svg.png"));

        ListDatos.add(new pais("Islas Salomón","<iframe width=\"400\" height=\"300\" src=\"https://youtu.be/O1v8NmtT43Q\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://youtu.be/O1v8NmtT43Q","https://es.wikipedia.org/wiki/Bandera_de_las_Islas_Salom%C3%B3n#/media/Archivo:Flag_of_the_Solomon_Islands.svg"));

        ListDatos.add(new pais("Kiribati","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=eYBx9vfk9ag\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=eYBx9vfk9ag","https://upload.wikimedia.org/wikipedia/commons/d/d3/Flag_of_Kiribati.svg"));

        ListDatos.add(new pais("Tonga","<iframe width=\"400\" height=\"300\" src=\"https://youtu.be/5MYgUPYaOk8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://youtu.be/5MYgUPYaOk8","https://upload.wikimedia.org/wikipedia/commons/9/9a/Flag_of_Tonga.svg"));

        ListDatos.add(new pais("Samoa","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=FJx2CLrEx7k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=FJx2CLrEx7k","https://upload.wikimedia.org/wikipedia/commons/3/31/Flag_of_Samoa.svg"));

        ListDatos.add(new pais("Tuvalu","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=KMNpPum256U\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=KMNpPum256U","https://upload.wikimedia.org/wikipedia/commons/3/38/Flag_of_Tuvalu.svg"));

        ListDatos.add(new pais("Vanuatu","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=JJzAYgUQib8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=JJzAYgUQib8","https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Vanuatu.svg/200px-Flag_of_Vanuatu.svg.png"));

        ListDatos.add(new pais("Micronesia","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=2FAkFHcDBWE\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=2FAkFHcDBWE","http://www.viajesoceania.com/wp-content/uploads/bandera-de-micronesia.gif"));

        ListDatos.add(new pais("Nauru ","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/watch?v=QAqLbXB-Jvw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/watch?v=QAqLbXB-Jvw","https://upload.wikimedia.org/wikipedia/commons/3/30/Flag_of_Nauru.svg"));



// Paises Euporeos

        ListDatos.add(new pais("Alemania","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/YYrXbV6hp7g\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/YYrXbV6hp7g","https://es.wikipedia.org/wiki/Bandera_de_Alemania#/media/Archivo:Flag_of_Germany.svg"));
        ListDatos.add(new pais("Andorra ","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/32ixbsBo1nk\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/32ixbsBo1nk","http://www.andorramania.net/images/bandera-de-andorra-640.jpg"));
        ListDatos.add(new pais("Armenia ","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/XAkO488S-Lg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/XAkO488S-Lg","https://es.wikipedia.org/wiki/Archivo:Flag_of_Armenia.svg"));
        ListDatos.add(new pais("Austria ","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/0pAZM2hgWkY\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/0pAZM2hgWkY","https://http2.mlstatic.com/bandera-de-austria-nueva-150x90cm-D_NQ_NP_877068-MLA27389339069_052018-F.jpg"));
        ListDatos.add(new pais("Bélgica ","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/jFA-PdqfbiM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/jFA-PdqfbiM","https://es.wikipedia.org/wiki/Archivo:Flag_of_Belgium.svg"));

    }
}
